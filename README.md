# Asp.net - introduction #

## How to use this repository? ##
All exercises we did during classes are on the branches called with pattern `exercise/x-subject`.
Please refer to presentation, those names are also used there.

## Presentation ##
You can find presentation on this repository in .pdf format, [in resources - click](resources/c-sharp_aspnet_introduction.pdf)

### How to download it? ###
![How to download presentation](resources/how-to-download-presentation.png)